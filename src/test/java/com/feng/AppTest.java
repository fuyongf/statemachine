package com.feng;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.feng.dto.DocDTO;
import com.feng.enums.AuditStateEnum;
import com.feng.service.IAuditStateManager;
import com.feng.service.impl.AuditStateManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        List<String> st = new ArrayList<>();
        DocDTO d =new DocDTO();
        d.setRemark("1");
        String r = d.getRemark();
        d.setRemark("2");
        st.add("qq");
        boolean a = st.contains("qq");
        a=st.contains("QQ".toLowerCase());
        assertTrue("01".startsWith("0"));
        assertEquals("1","02".substring(1));
        assertTrue( true );
    }

    @Test
    public void testRemove7(){
        List<String> strings = new ArrayList<>();
        strings.add("a");
        strings.add("b");
        strings.add("c");
        strings.add("d");

        Iterator<String> iterator = strings.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();
            iterator.remove();
        }

        System.out.println(strings);
    }
    private static boolean checkIdNumber(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str.charAt(0)+"");
        return isNum.matches();
    }
    @Autowired
    private IAuditStateManager auditStateManager;
    @Test
    public void audit(){
        auditStateManager.audit("test", AuditStateEnum.ONE,AuditStateEnum.TWO,null);
    }
}
