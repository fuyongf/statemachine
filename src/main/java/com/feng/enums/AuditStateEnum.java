package com.feng.enums;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>stateMachine</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-08-12 08:27
 * @note 无。
 * @since 1.0.0
 */
public enum AuditStateEnum {
    /**
     * 待初审
     */
    ONE("待初审", "1")
    /**初审违规*/
    , TWO("初审违规", "2")
    /**初审正常*/
    , THREE("初审正常", "3")
    /**待申诉*/
    , FOUR("待申诉", "4")
    /**发起申诉*/
    , FIVE("发起申诉", "5")
    /**放弃申诉*/
    , SIX("放弃申诉", "6")
    /**待复审*/
    , SEVEN("待复审", "7")
    /**复审违规*/
    , EIGHT("复审违规", "8")
    /**复审正常*/
    , NINE("复审正常", "9")
    /**已关闭*/
    , EIGHTEEN("已关闭", "18")
    /**待终审*/
    , TWENTY_ONE("待终审", "21")
    /**终审正常*/
    , TWENTY_TWO("终审正常", "22")
    /**终审违规*/
    , TWENTY_THREE("终审违规", "23");
    /**
     * 名称
     */
    private String name;
    /**
     * 值
     */
    private String value;

    AuditStateEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }
}
