package com.feng.service;

import com.feng.dto.result.OperateResult;
import com.feng.enums.AuditStateEnum;

import java.util.Map;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>stateMachine</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-08-12 08:37
 * @note 无。
 * @since 1.0.0
 */
public interface IAuditStateService {
     OperateResult audit(String id, AuditStateEnum nextState, Map otherData);
     OperateResult rollback(String id, AuditStateEnum currentState, Map otherData);
}
