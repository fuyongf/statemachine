package com.feng.service.impl;

import com.feng.config.SpringContextHolder;
import com.feng.dto.result.OperateResult;
import com.feng.enums.AuditStateEnum;
import com.feng.service.IAuditStateManager;
import com.feng.service.IAuditStateService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>stateMachine</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-08-12 10:12
 * @note 无。
 * @since 1.0.0
 */
@Component
public class AuditStateManager implements IAuditStateManager {

    private IAuditStateService getAuditStateService(AuditStateEnum state) {
        return SpringContextHolder.getBean(state + "AuditStateService");
    }

    @Override
    public OperateResult audit(String id, AuditStateEnum currentState, AuditStateEnum nextState, Map otherData) {
        if (!StringUtils.hasText(id)) {
            return new OperateResult(1, "id is not null");
        }
        IAuditStateService auditStateService = getAuditStateService(currentState);
        return auditStateService.audit(id, nextState, otherData);
    }

    @Override
    public OperateResult rollback(String id, AuditStateEnum currentState, AuditStateEnum preState, Map otherData) {
        if (!StringUtils.hasText(id)) {
            return new OperateResult(1, "id is not null");
        }
        //回滚状态处理器
        IAuditStateService auditStateService = getAuditStateService(preState);
        return auditStateService.rollback(id, currentState, otherData);
    }
}
