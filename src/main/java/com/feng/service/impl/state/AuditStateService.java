package com.feng.service.impl.state;

import com.feng.dto.DocDTO;
import com.feng.dto.result.OperateResult;
import com.feng.enums.AuditStateEnum;
import com.feng.service.IAuditor;
import com.feng.service.IAuditStateService;

import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>stateMachine</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-08-12 09:05
 * @note 无。
 * @since 1.0.0
 */
public abstract class AuditStateService implements IAuditStateService {
    //当前状态
    private AuditStateEnum state;
    //后续可变状态组
    private List<AuditStateEnum> nextStates;

    protected AuditStateService(AuditStateEnum currentState, List<AuditStateEnum> nextStates) {
        this.state = currentState;
        this.nextStates = nextStates;
    }

    @Override
    //@Transactional
    public OperateResult audit(String id, AuditStateEnum nextState, Map otherData) {
        return handle(id, this::auditInternal, nextState, otherData);
    }

    @Override
    public OperateResult rollback(String id, AuditStateEnum currentState, Map otherData) {
        return handle(id, this::rollbackInternal, currentState, otherData);
    }

    private OperateResult handle(String id, IAuditor IAuditor, AuditStateEnum state, Map otherData) {
        if (!nextStates.contains(state)) {
            return new OperateResult(2, String.format("当前状态 %s不能转换为 %s", state.getName(), state.getName()));
        }
        //根据id及当前状态获取doc
        DocDTO doc = new DocDTO();
        if (doc == null) {
            return new OperateResult(3, String.format("查询不到当前状态%s的单据%s", state.getName(), id));
        }
        OperateResult result = IAuditor.audit(doc, state, otherData);
        if (result.isSuccess()) {
            //插入状态变更记录等操作
        }
        return result;
    }

    protected abstract OperateResult auditInternal(DocDTO doc, AuditStateEnum nexState, Map otherData);

    protected abstract OperateResult rollbackInternal(DocDTO doc, AuditStateEnum currentState, Map otherData);
}
