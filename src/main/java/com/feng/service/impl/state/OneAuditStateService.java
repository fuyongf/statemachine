package com.feng.service.impl.state;

import com.feng.consts.SystemConst;
import com.feng.dto.DocDTO;
import com.feng.dto.result.OperateResult;
import com.feng.dto.result.OperateResultExt;
import com.feng.enums.AuditStateEnum;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.io.Console;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>stateMachine</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-08-12 09:03
 * @note 无。
 * @since 1.0.0
 */
@Service("ONEAuditStateService")
public class OneAuditStateService extends AuditStateService {
    final static List<AuditStateEnum> nextStates = Arrays.asList(AuditStateEnum.THREE, AuditStateEnum.TWO);

    public OneAuditStateService() {
        super(AuditStateEnum.ONE, nextStates);
    }

    @Override
    protected OperateResult auditInternal(DocDTO doc, AuditStateEnum nexState, Map otherData) {
        if (otherData == null || !otherData.containsKey(SystemConst.CODE)) {
            return new OperateResult(999, "缺少必要的参数");
        }

        doc.setAuditState(nexState);
        doc.setRemark(otherData.get(SystemConst.CODE).toString());
        //根据当前状态和id变更状态 update
        return new OperateResultExt<DocDTO>(0, "成功", doc);
    }

    @Override
    protected OperateResult rollbackInternal(DocDTO doc, AuditStateEnum currentState, Map otherData) {
        return new OperateResult(0,"回滚成功");
    }
}
