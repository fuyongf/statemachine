package com.feng.service;

import com.feng.dto.DocDTO;
import com.feng.dto.result.OperateResult;
import com.feng.enums.AuditStateEnum;

import java.util.Map;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>statemachine.git</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-09-01 09:47
 * @note 无。
 * @since 1.0.0
 */
@FunctionalInterface
public interface IAuditor {
    OperateResult audit(DocDTO doc, AuditStateEnum nexState, Map otherData);
}
