package com.feng.dto.result;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>stateMachine</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-08-12 08:38
 * @note 无。
 * @since 1.0.0
 */
public class OperateResultExt<T> extends OperateResult {

    public OperateResultExt(int code) {
        super(code);
    }

    public OperateResultExt(int code, String msg, T value) {
        super(code, msg);
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    private T value;

}
