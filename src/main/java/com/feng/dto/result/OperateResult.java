package com.feng.dto.result;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>stateMachine</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-08-12 08:45
 * @note 无。
 * @since 1.0.0
 */
public class OperateResult {
    private int code;
    private String message;
    static final int SUCCESS = 0;

    public OperateResult(int code) {
        this.code = code;
    }

    public OperateResult(int code, String msg) {
        this.code = code;
        this.message = msg;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return code == SUCCESS;
    }
}
