package com.feng.common;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>statemachine.git</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-09-09 08:26
 * @note 无。
 * @since 1.0.0
 */
@Data
public class BaseDomain implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private Date createTime;
    private Date updatedTime;
    private boolean isDeleted;
}
