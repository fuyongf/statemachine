package com.feng.common;

import lombok.Data;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>statemachine.git</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-09-09 08:28
 * @note 无。
 * @since 1.0.0
 */
@Data
public class BaseOperatorDomain extends BaseDomain {
    private String creatorId;
    private String creatorName;
    private String operatorId;
    private String operatorName;
}
