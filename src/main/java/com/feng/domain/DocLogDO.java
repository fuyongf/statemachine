package com.feng.domain;

import com.feng.common.BaseOperatorDomain;
import com.feng.enums.AuditStateEnum;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>statemachine.git</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-09-10 17:21
 * @note 无。
 * @since 1.0.0
 */
public class DocLogDO extends BaseOperatorDomain {
    private String docId;
    private String remark;
    private String Title;
    private AuditStateEnum currentState;
    private AuditStateEnum nextState;
}
