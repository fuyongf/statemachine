package com.feng.domain;

import com.feng.common.BaseOperatorDomain;
import lombok.Data;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>statemachine.git</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-09-10 16:20
 * @note 无。
 * @since 1.0.0
 */
@Data
public class ApproverDO extends BaseOperatorDomain {
    private String name;
    private String docId;
    private String approverId;
    private String approverRole;
}
