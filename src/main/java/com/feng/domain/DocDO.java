package com.feng.domain;

import com.feng.common.BaseOperatorDomain;
import com.feng.enums.AuditStateEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * Copyright (C) 2020 Kingstar Winning, Inc. All rights reserved.
 * <p>statemachine.git</p>
 *
 * @author Fuyongfeng（fuyongfeng@tech-winning.com）
 * @date 2021-09-02 17:33
 * @note 无。
 * @since 1.0.0
 */
@Data
public class DocDO extends BaseOperatorDomain  {
    private String id;
    private String title;
    private String body;
    private String remark;
    private AuditStateEnum auditState;
}
